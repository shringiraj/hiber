package com.shri.hiber;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class App {
	private static Session session;

	public static void main(String[] args) {
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		session = factory.openSession();

		Transaction tx = session.beginTransaction();
		getStudent(1);
		getStudent(2);
		getStudent(3);
		Student student = getStudent(4);
		System.out.println(student.getStudentCourse());
		// addStudent();

		tx.commit();
		session.close();

	}

	public static void addStudent() {
		Student student = new Student();
		student.setId(1);
		student.setName("Shringiraj");
		student.setEmail("Shringiraj@gmail.com");
		student.setBranch("CS");
		student.setDob(new Date());
		session.save(student);

		StudentCourse c1 = new StudentCourse("Sample", student);
		StudentCourse c2 = new StudentCourse("Sample", student);
		session.save(c1);
		session.save(c2);
	}

	public static Student getStudent(int id) {
		return session.get(Student.class, id);
	}
}
